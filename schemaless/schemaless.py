#!/usr/bin/env python
from __future__ import print_function

from twisted.internet import reactor, defer
from twisted.enterprise import adbapi
from twisted.python import filepath, log

import re
import uuid
import string
import random
import itertools
import collections

from flattendict import flattenDict

class Schemaless(object):

    dbpool = adbapi.ConnectionPool('sqlite3', 'memory.db', check_same_thread=False)
    dbpool.runOperation('PRAGMA foreign_keys=ON')
    id_field = '__id'

    def query(self, table, query):
        '''
        query: 
            test:
               c: 
                  x:
                     'like "%hello%"'
        

        query:
            items:
                amount: '> 1000'
                product:
                    ean: 'like "P2%"'
        
        db = Schemaless()
        for bill in db.query('bills', {
            "items": {
                "amount": "> 1000",
                "product": {
                    "ean": "like 'P2%'
                }
            }
        }):
             [ (bill['id'], item, product) for item in bill['items'] for product in bill['items']['product'] ]

        '''
        pass

    def runOperation(self, *args, **kwargs):
        log.err('runOperation: {} {}'.format(args, kwargs))
        return self.dbpool.runOperation(*args, **kwargs)

           

    clean_field_re = re.compile(r'^[a-zA-Z_]+\w+$')
    clean_field_sub_re = re.compile(r'\W+')
    def _sanitize_fields(self, *fields):
        '''
        make field sql friendly
        store mapping in db
        '''
        for field in fields:
            if self.clean_field_re.match(field) is None:
                rnd = ''.join([ random.choice(string.digits) for i in range(6) ] )
                clean_field =  '__SUB_{}_{}'.format(self.clean_field_sub_re.sub('', field), rnd)
                print('replace {} => {}'.format(field, clean_field))
                yield clean_field
            else:
                yield field

    # ctable = list(self._sanitize_fields(ctable))[0]
    # fields = list(self._sanitize_fields(*fields))

    def _get_insertrows_sql(self, table, row):
        fields, vals = zip(*row.items())
        fieldstr = ','.join(fields)
        qstr = ','.join('?'*len(fields))
        sql = 'insert into {} ({}) values ({})'.format(table, fieldstr, qstr)
        return sql, vals

    def insert(self, table, data):
        flattened = self._flatten_unroll(table, data)
        rowDefs = []
        for ctable, row in flattened.unrolled:
            sql, vals = self._get_insertrows_sql(ctable, row)
            d = self.runOperation(sql, vals)
            d.addCallback(self._insert_success, flattened.insert_id)
            d.addErrback(self._insert_failed, flattened, ctable, sql, vals)
            d.addCallback(self._insert_success, flattened.insert_id)
            d.addErrback(log.err)
            rowDefs.append(d)
        rowDefList = defer.DeferredList(rowDefs)
        rowDefList.addCallback(lambda res: res[0][1])
        return rowDefList

    def _insert_success(self, _, insert_id):
        print('_insert_success insert_id={}'.format(insert_id))
        return insert_id
        #try: rowDef.callback(insert_id)
        #except defer.AlreadyCalledError: pass

    def _insert_failed(self, err, flattened, ctable, sql, vals):
        msg = err.getErrorMessage()
        d = None
        if msg.startswith('no such table:'):
            d = self._insert_failed_no_table(msg, ctable)
            d.addCallback(lambda _: self.runOperation(sql, vals))
            d.addErrback(self._insert_failed, flattened, ctable, sql, vals)
        elif msg.find('has no column named') != -1:
            d = self._insert_failed_no_column(msg, ctable, flattened.foreign_keys)
            d.addCallback(lambda _: self.runOperation(sql, vals))
            d.addErrback(self._insert_failed, flattened, ctable, sql, vals)
        else:
            err.printTraceback()
            reactor.stop()
        return d
            
    def _insert_failed_no_table(self, msg, ctable):
        create_sql = 'create table if not exists {} ({}, primary key({}))'.format(ctable, self.id_field, self.id_field)
        return self.runOperation(create_sql)

    def _insert_failed_no_column(self, msg, ctable, foreign_keys):
        field = msg.rsplit()[-1]
        alter_sql = 'alter table {} add column {}'.format(ctable, field)
        if field in foreign_keys[ctable]:
            alter_sql += ' references {}({}) on delete cascade'.format(*foreign_keys[ctable][field])
        return self.runOperation(alter_sql)
 
    def _get_table_idx(self, k):
        if isinstance(k, tuple):
            return k
        return k, None

    Flattened = collections.namedtuple('Flattened', ['insert_id', 'foreign_keys', 'unrolled' ])
    def _flatten_unroll(self, table, data):
        flattened = flattenDict(data, lift=lambda x: (x,))
        unrolled = []
        foreign_keys = collections.defaultdict(dict)
        flattened = ( (self._sanitize_path(*path), v) for path, v in flattened )
        insert_id = self._unroll_flattened_recursive(table, flattened, unrolled, foreign_keys)
        return self.Flattened(insert_id=insert_id, foreign_keys=foreign_keys, unrolled=unrolled)

    def _sanitize_path(self, *path):
        print('_sanitize_path: {}'.format(path))
        return path
 
    def _unroll_flattened_recursive(self, table, flattened, unrolled, foreign_keys):

        #import pdb; pdb.set_trace()

        flattened = itertools.tee(flattened, 2)

        values = { self._get_table_idx(path[0])[0]: v for path, v in flattened[0] if len(path) is 1 }
            
        values[self.id_field] = uuid.uuid4().int % 9223372036854775807
        unrolled.append( (table, values) )

        nested = ( (k, v) for k, v in flattened[1] if len(k) > 1 )
        nested = sorted(nested, key=lambda kv: kv[0][0])

        for child_table, g in itertools.groupby(nested, key=lambda kv: kv[0][0]):
            g = list(g)
            if isinstance(child_table, tuple) and child_table[-1] is list:
                child_table, idx = '{}Array'.format(child_table[0]), child_table[1]
            child_flattened = [ (k[1:], v) for k, v in g ]
            child_fkey_col = '{}{}'.format(table, self.id_field)

            child_flattened.append( ( (child_fkey_col,), values[self.id_field],) )
            child_table = '{}_{}'.format(table, child_table)
            foreign_keys[child_table][child_fkey_col] = (table, self.id_field,)

            self._unroll_flattened_recursive(child_table, child_flattened, unrolled, foreign_keys)

        return values[self.id_field]

    def query(self, sql):
       return self.dbpool.runQuery(sql)
       
if __name__ == '__main__':
    db = Schemaless()
    data = {
        'a': { 'aa': 1 },
        'b': 2,
        'c': [ { 'x': 1 }, { 'x': 2, 'y': 10}, {'x': 3, 'z': 50}],
        # 'd': [ 1, 2, 3, 4, 5],
        'e':{
        }
    }

    # data = {"items":[{"code":"P2","qty":1,"ts":"j00q750x","product":{"name":"Bournvita","app":"folder","code":"P2","price":150,"ean":"P2","eants":"P2","stock":"normal"},"amount":150}],"amount":150,"payitems":[],"paid":0,"customer":False,"phone":False,"ts":"j00q70pm","billno":0,"date":"8/3/2017, 2:44:03 pm","delivery":{"name":"Ramu","deliveryId":"PD1001","dispatched":"2017-03-08T09:14:44.393Z"}}

    # sqlite> select * from test inner join test_a on test.id == test_a.test_id inner join test_c on test.id == test_c.test_id inner join test_c_c on test_c.id == test_c_c.test_c_id;

    import sys
    log.startLogging(sys.stderr)

    def insert_done(insert_id):
        log.err('insert_done id={}'.format(insert_id))

    @defer.inlineCallbacks
    def _fromMongoInsert():
        import txmongo
        mongo = yield txmongo.MongoConnection()
        
        bills = mongo.bills.bills  # `foo` database
        
        # fetch some documents
        docs = yield bills.find(limit=1)
        for doc in docs:
            print(doc)
            d = db.insert('test', data)
            d.addCallback(insert_done)
            d.addErrback(log.err)

    @defer.inlineCallbacks
    def _fromTestData():
        d = db.insert('test', data)
        d.addCallback(insert_done)
        d.addErrback(log.err)
    
    # reactor.callLater(0, _fromMongoInsert)
    reactor.callLater(0, _fromTestData)

    reactor.run()
