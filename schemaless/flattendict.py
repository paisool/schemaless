#!/usr/bin/env python
# http://stackoverflow.com/a/6043835
# TODO:
#   verify sql statements

from collections import Mapping, namedtuple, OrderedDict, defaultdict
from itertools import chain
from operator import add
import re
import uuid
import numbers
import zlib
import sqlite3
import pickle

_FLAG_FIRST = object()

class Sanitizer:
    def __init__(self):
        self._sane = {}
        
    def sane(self, name):
        sqlname = re.sub(r'\W+', '', name)
        if name != sqlname:
            self._sane[sqlname] = name
        return sqlname

class SqlSchema:
    def __init__(self, dbname=None):
        dbname = ':memory:' if dbname is None else dbname
        self.db = sqlite3.connect(dbname)
        self.schema = defaultdict(dict)
        self._pending = []

    def ensure_columns(self, table, cols, refFieldInfo):
        refFieldName, parentTable, id_field = refFieldInfo
        print('ensure_columns table={} cols={} ref={} {} {}'.format(table, cols, refFieldName, parentTable, id_field))
        for col in cols:
            if table in self.schema and col in self.schema[table]: # TODO: cases for changing type of values
                continue
            
            sqlconstraint = ''
            if col == refFieldName:
                sqlconstraint = 'references {}({}) on delete cascade'.format(parentTable, id_field) 
            elif col == id_field:
                sqlconstraint = 'primary key'
            if table not in self.schema:
                self._pending.append('create table if not exists {} ({} primary key)'.format(table, id_field))
            if col != id_field:
                self._pending.append('alter table {} add column {} {} '.format(table, col, sqlconstraint))
            self.schema[table][col] = sqlconstraint

sqlschema = SqlSchema('test.db')

id_field = '_id'
def _sqlobjects_r(datastore, obj, join=add, lift=lambda x:(x,)):
    sanitizer = Sanitizer()
    def visit(subdict, partialKey, parentId=None, parentTable=None):
        _id = uuid.uuid4().int % 9223372036854775807 
        tableData = dict({ id_field: _id })
        tableName = '{}_{}'.format(datastore, '_'.join(partialKey)) if not partialKey is _FLAG_FIRST else datastore
        tableName= sanitizer.sane(tableName)
        refFieldName = None
        if parentId:
            refFieldName = '{}{}'.format(parentTable, id_field)
            tableData[refFieldName] = parentId
        for k,v in sorted(subdict.items(), key=lambda o: o[0]):
            newKey = lift(k) if partialKey==_FLAG_FIRST else join(partialKey,lift(k))
            if isinstance(v,Mapping):
                yield from visit(v, newKey, parentId=_id, parentTable=tableName)
            elif isinstance(v, list) or isinstance(v, tuple):
                for i, dv in enumerate(v):
                    sd = { k: dv}
                    yield from visit(sd, newKey, parentId=_id, parentTable=tableName)
            elif isinstance(v, str) or isinstance(v, numbers.Integral) or isinstance(v, bytes):
                tableData[sanitizer.sane(k)] = v
            else:
                v = zlib.compress(pickle.dumps(v)) # FIXME: pickled functions
                tableData[sanitizer.sane(k)] = v
        yield tableName, tableData, (refFieldName, parentTable, id_field)
    yield from visit(obj, _FLAG_FIRST)
    print('sanitizer {}'.format(sanitizer._sane))

def sqlobjects(datastore, testData):
    cursor = sqlschema.db.cursor()
    for table, data, refFieldInfo in _sqlobjects_r(datastore, testData, lift=lambda x:(x,)):
        sqlschema.ensure_columns(table, data.keys(), refFieldInfo)
        sql = ';\n'.join(sqlschema._pending)
        sqlschema.db.executescript(sql)
        sqlschema._pending = []
        yield table, data
        fields = data.keys()
        values = [ ':{}'.format(f) for f in fields ]
        cursor.execute('insert into {} ({}) values ({})'.format(table, ', '.join(fields), ', '.join(values)), data)
    sqlschema.db.commit()

if __name__ == '__main__':
    testData = {
        '@a':1,
        'b':2,
        'c':{
            'aa':3,
            'bb':[ dict(a=22), dict(a=23, b=24), dict(b=25, c=26), 27, None ],
            'cc':{
                '@a@a@a': dict(x='101010101010101010101011010101010110101010101101010101')
            }
        },
        'd': [1, 2, 3],
        'e': 5
    }
    from pprint import pprint as pp
    for table, data in sqlobjects('test', testData):
        print('*** {} => {}'.format(table, data))
    # import code; code.interact(local=locals())
